# SpinSpire traefik setup

This project allows you to run traefik in docker such that it binds to
port 80 and 443 and acts as the reverse proxy on the box. It then
routes HTTP(S) requests to _docker containers_ running on the box.

It also can route requests to _HTTP listeners running on the host_.
See `config/http.yml` for details.

Another feature is automatic HTTPS. It provisions TLS certs from Letsencrypt using Traefik's ACME cert resolver.

# How to use

1. Copy `.env.sample` to `.env` and edit it.
1. Check all the other config files. Main ones are ...

    1. `traefik.yml`: static config
    1. `config/*.yml`: dynamic config
    1. `docker-compose.yml`: you usually don't edit this file, but it is essential.
    1. `docker-compose.override.yml`: in case you want to override some `docker-compose.yml` settings just for your environment.

1. Create "web" network ...
   ```
   docker network create web
   ```
1. Start container ...
   ```
   docker-compose up -d
   ```
1. Check the logs ...
   ```
   docker-compose logs -f
   ```
1. Now go ahead and start other docker containers. These containers should expose `labels` like this ...
```
labels:
  - traefik.enable=true
  - traefik.http.routers.foorouter.rule=Host(`${HTTP_HOSTNAME}`)
```

# Misc.

1. `acme.json`: used by traefik's acme TLS resolver (Letsencrypt) to store certs and keys. It will be created if it doesn't already exist. Or you can create it as empty JSON file (put `{}` into it).